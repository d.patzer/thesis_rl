
#!/usr/bin/env python3
import gym
import torch as T
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np

class PolicyNetwork(nn.Module):
    def __init__(self, ALPHA, input_dims, n_actions):
        super(PolicyNetwork, self).__init__()

        self.input_dims = input_dims
        self.n_actions = n_actions
        self.fc1 = nn.Linear(*self.input_dims, 16)
        self.fc2 = nn.Linear(16, 16)
        self.fc3 = nn.Linear(16, 16)
        self.fc4 = nn.Linear(16, n_actions)
        self.optimizer = optim.Adam(self.parameters(), lr=ALPHA)
        self.device = T.device('cuda' if T.cuda.is_available() else 'cpu')
        self.to(self.device)

    def forward(self, observation):
        state = T.Tensor(observation).to(self.device)
        x = F.relu(self.fc1(state))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        probabilities = F.softmax(self.fc4(x), dim=0)
        return probabilities

class REINFORCE_Agent(object):
    def __init__(self, ALPHA, input_dims, GAMMA, n_actions):
        self.gamma = GAMMA
        self.reward_memory = []
        self.action_memory = []
        self.policy = PolicyNetwork(ALPHA, input_dims, n_actions)

    def get_action(self, observation):

        action_probs = T.distributions.Categorical(self.policy.forward(observation))
        action = action_probs.sample()
        log_probs = action_probs.log_prob(action)
        self.action_memory.append(log_probs)

        return action.item()

    def store_rewards(self, reward):
        self.reward_memory.append(reward)

    def learn(self):
        # In monte carlo style approaches the agent learns at the end of each episode based on the experience
        # it accumulated. And performs actions according to some probability distribution
        # in order to calculate the returns (sum of discounted future rewards)
        # at each time step you have to iterate over the
        # agents memory of the episode

        self.policy.optimizer.zero_grad() # prevent accumulation of gradients during training
        # Assumes only a single episode for reward_memory
        G = np.zeros_like(self.reward_memory, dtype=np.float64) # store the discounted future rewards accumulated during the episode
        for t in range(len(self.reward_memory)):
            G_sum = 0
            discount = 1
            for k in range(t, len(self.reward_memory)):
                G_sum += self.reward_memory[k] * discount
                discount *= self.gamma
            G[t] = G_sum
            # because the rewards can have a high variance we want to subtract the
            # mean and devide it by the std. diviation
        mean = np.mean(G)
        std = np.std(G) if np.std(G) > 0 else 1
        G = (G - mean) / std

        G = T.tensor(G, dtype=T.float).to(self.policy.device)

        # next calculate the loss
        loss = 0
        for g, logprob in zip(G, self.action_memory):
            loss += -g * logprob

        loss.backward()
        self.policy.optimizer.step()

        self.action_memory = []
        self.reward_memory = []

if __name__ == '__main__':
    agent = REINFORCE_Agent(ALPHA=0.001, input_dims=[8], GAMMA=0.99, n_actions=4)

    env = gym.make('LunarLander-v2')
    score_history = []
    score = 0
    n_actions = 4
    num_episodes = 2500


    for i in range(num_episodes):
        print('episode: ', i,'score: ', score)
        done = False
        score = 0
        observation = env.reset()
        while not done:
            action = agent.get_action(observation)
            observation_, reward, done, info = env.step(action)
            agent.store_rewards(reward)
            observation = observation_
            score += reward
        score_history.append(score)
        agent.learn()

   # filename = 'lunar-lander-alpha001-128x128fc-newG.png'
    plotLearning(score_history, filename=filename, window=25)
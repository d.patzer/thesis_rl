# Multi-hop reasoning in knowledge graphs using reinforcement learning 


## About

This Repository is supposed to keep track of the whole project life cycle. Mainly designed to make it more intuitive for Mr. Bank, Mr. Moore and Mr. Himmelsbach to review the project and recieve updates on current challenges.
The Gantt-Chart below provides an overview of the project phases.

Each phase includes generic tasks that will be explained in greater detail in the table below. Since the project is designed to use agile project management techniques only a rough syllabus is defined in advance. The table will be updated to allow the project to be agile. Updates will be made at the beginning of each phase. Tasks will be added when entering a new phase to fulfill the specific following milestone requirements. Similar to an initial sprint planning meeting. Every generic task will be marked once it is completed.  


## Generic Task Discription 

| Phase                                    | Task                  | Discription                   | Links     | Done  |
| ---------------------------------------- | --------------------- |----------------------------   | -------   | ---|
| Introduction to reinforcement learning   | Standford CS234: RL   | Introduction to Reinforce Learning online lecture (Winter 2019) | [Youtube](https://www.youtube.com/watch?v=FgzM3zpZ55o&list=PLoROMvodv4rOSOPzutgyCTapiGlY2Nd8u&index=1)|X
|                                          | Inverse Pendulum      | Pytorch Tutorial to train a deep train a Deep Q Learning (DQN) agent on an inverse pendulum | [Pytorch.org](https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html) |X
|                                          |  Q-Learning with OpenAI Taxi | Train agent to pick up the passenger at one location and drip him off to the goal as fast as possible | [Github](https://github.com/simoninithomas/Deep_reinforcement_learning_Course/blob/master/Q%20learning/Taxi-v2/Q%20Learning%20with%20OpenAI%20Taxi-v2%20video%20version.ipynb) | X
|                                          |Q-Learning with FrozenLake | Train agent to navigate from the starting state to the goal state | [Github](https://github.com/simoninithomas/Deep_reinforcement_learning_Course/blob/master/Q%20learning/FrozenLake/Q%20Learning%20with%20FrozenLake.ipynb)| X
| State-of-the-art research                 |Policy Gradient methodes | Policy Gradient methodes for reinforcement learning with funktion approximation| [Sutton](https://papers.nips.cc/paper/1713-policy-gradient-methods-for-reinforcement-learning-with-function-approximation.pdf)| X
|                                           | A2C, PPO , TRPO, DDPG and TD3 | Different policy algorithms and their integration in pytorch | [Schulman (TRPO)](https://arxiv.org/abs/1502.05477)| X
|                                           | MINERVA algorithm      | review the walk-based QA framework and the on-policy reinforcement learning approach proposed by MINERVA |[Paper](https://arxiv.org/abs/1711.05851)|X| 
|                                           | Deeppath          | A reinforcement learning method for knowledge graph reasoning    | [Paper](https://arxiv.org/abs/1707.06690) |X|
|                                           | Salesforce     | Multi-Hop Knowledge Graph Reasoning with Reward Shaping | [Paper](https://www.aclweb.org/anthology/D18-1362/) |X|
|                                           |HybridNLP2018 | Tutorial on Hybrid Techniques for Knowledge-based NLP| [Github](https://github.com/HybridNLP2018/tutorial)
|                                           |Relational Machine Learning| A Review of Relational Machine Learning for Knowledge Graphs | [Paper](https://www.dbs.ifi.lmu.de/~tresp/papers/1503.00759v3.pdf)
| Conceptualisation                         |  data acquisition  | Representing the data in a knowledge graph                   | [Grakn.ai](https://grakn.ai)          |      |X
| Programming                               | RL-Libaries| open-source liabries to develop state-of-the-art RL models|  [ChainerRL](https://github.com/chainer/chainerrl), [Bair](https://github.com/astooke/rlpyt) [ReAgent](https://github.com/facebookresearch/ReAgent)| [MyGithub](https://github.com/patzaa/MultiHopKG) |X|
|                                           | code development     | Developing a reinforcement learning agent for multi-hop reasoning in knowledge graphs | ||
| Testing                                   | Benchmark tests and evaluation | between different models/approaches |||X|
| Writing                                   | ||||
|Correction|||||
